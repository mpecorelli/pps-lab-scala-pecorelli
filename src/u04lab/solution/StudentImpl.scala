package u04lab.solution

import scala.collection.mutable

/**
  * Created by margherita on 23/03/17.
  */
trait Student {
  def name: String
  def year: Int
  def enrolling(course: Course*): Unit
  def courses: Set[String]
  def hasTeacher(teacher: String): Boolean
}

trait Course {
  def name: String
  def teacher: String
}

object Student {
  def apply(name: String, year: Int = 2017):Student = StudentImpl(name, year)
}

object Course {
  def apply(name: String, teacher: String):Course = CourseImpl(name, teacher)
}

case class StudentImpl(name: String, year: Int) extends Student {

  private val coursesSet: mutable.Set[Course] = mutable.Set()

  //override def enrolling(course: Course): Unit = coursesSet += course

  override def enrolling(course: Course*): Unit = {
    for (i <- course.length-1 to 0 by -1) coursesSet += course(i)
  }

  override def courses: Set[String] = coursesSet map (i => i.name) toSet

  override def hasTeacher(teacher: String): Boolean = (coursesSet map (_ => teacher) find (x => x == teacher) size) != 0
}

case class CourseImpl(name: String, teacher: String) extends Course {}

object Try extends App {
  val cPPS = Course("PPS","Viroli")
  val cPCD = Course("PCD","Ricci")
  val cSDR = Course("SDR","D'Angelo")
  val s1 = Student("mario",2015)
  println(s1)
  val s2 = Student("gino",2016)
  println(s2)
  val s3 = Student("rino") //defaults to 2017
  println(s3)
/*  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR) */

  s1.enrolling(cPPS, cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS, cPCD, cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Set(PPS, PCD),Set(PPS),Set(PPS, PCD, SDR))
  println(s1.hasTeacher("Ricci")) // true
}

