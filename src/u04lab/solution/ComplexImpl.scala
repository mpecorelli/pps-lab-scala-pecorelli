package u04lab.solution

/**
  * Created by margherita on 23/03/17.
  */
trait Complex {
  def re: Double
  def im: Double
  def +(c: Complex): Complex
  def *(c: Complex): Complex
}

object Complex {
  def apply(re:Double, im:Double):Complex = ComplexImpl(re, im)
}

case class ComplexImpl(re: Double, im: Double) extends Complex{

  override def +(c: Complex): Complex = Complex(re+c.re, im+c.im)

  override def *(c: Complex): Complex = Complex((re*c.re)-(im*c.im), (im*c.re)+(re*c.im))
}

object TryComplex extends App {
  val a = Array(ComplexImpl(10,20), ComplexImpl(1,1), ComplexImpl(7,0))
  val b = ComplexImpl(10, 20)
  assert(a(0) == b)
  val c = a(0)+a(1)+a(2)
  println(c, c.re, c.im) // (ComplexImpl(18.0,21.0),18.0,21.0)
  val c2 = a(0)*a(1)
  println(c2, c2.re, c2.im) // (ComplexImpl(-10.0,30.0),-10.0,30.0)
  //check that equality and toString do not work
  //assert(Complex(10,20) != Complex(10,20))
  //check that equality and toString now work
  assert(Complex(10,20) == Complex(10,20))
}


